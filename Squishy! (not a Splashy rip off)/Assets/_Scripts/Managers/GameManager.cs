﻿using EncryptionTool;
using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField, FindAssets, Reorderable(false, options: ArrayOption.DisableSizeField)]
    private LevelModel[] _levels;

    [SerializeField, FindObject] private BallBehaviour _ballBehaviour;
    [SerializeField, FindObject] private CameraBehaviour _cameraBehaviour;

    [SerializeField, FindObject] private EncryptionInitializer _encryptionInitializer;

    [SerializeField] private GameObject _startLabel;

    private bool m_isInit;
    private bool m_isPlaying;

    private void Start()
    {
        Instance = this;

        m_isInit = false;
        m_isPlaying = false;

        SavedDataServices.Init(_encryptionInitializer);
        SavedDataServices.LoadEverythingFromLocal();
        
        LoadLevel(SavedDataServices.LevelIndex);
    }

    public void InitBehaviours()
    {
        m_isInit = true;

        var ballPosition = _ballBehaviour.Init();
        _cameraBehaviour.Init(ballPosition);
    }

    public void Play() => Play(true);

    public void Pause() => Play(false);

    private void Play(bool play)
    {
        m_isPlaying = play;
        _startLabel.SetActive(!play);
    }

    private void Update()
    {
        if (!m_isInit) return;

        if (Input.GetMouseButtonDown(0)) Play();

        if (!m_isPlaying) return;

        var deltaTime = Time.deltaTime;
        var ballPosition = _ballBehaviour.UpdateBall(deltaTime);
        _cameraBehaviour.UpdateCamera(ballPosition, deltaTime);
    }

    public void LoadNextLevel()
    {
        var index = SavedDataServices.LevelIndex;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        LoadLevel(++index);
    }

    public void LoadLevel(byte index, bool saveProgression = true)
    {
        if (index >= _levels.Length) index = 0;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        if (saveProgression) SavedDataServices.LevelIndex = index;

        SceneManager.LoadScene(_levels[index].SceneIndex, LoadSceneMode.Additive);
    }

    public void UnloadLevel(byte index) => SceneManager.UnloadSceneAsync(_levels[index].SceneIndex);

    public bool IsLevelLoaded(byte index) => _levels[index].IsLoaded();

    public void ReloadLevel() => LoadLevel(SavedDataServices.LevelIndex, false);
}