﻿using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public float zOffset;
    public float moveSpeed;

    private float m_xPosition;

    private Transform m_transform;

    public void Init(Vector3 targetPosition)
    {
        m_transform = transform;
        
        m_xPosition = targetPosition.x;
        SetPosition(targetPosition);
    }

    public void UpdateCamera(Vector3 targetPosition, float deltaTime)
    {
        m_xPosition = Mathf.Lerp(m_xPosition, targetPosition.x, moveSpeed * deltaTime);
        SetPosition(targetPosition);
    }

    private void SetPosition(Vector3 position) => 
        m_transform.position = new Vector3(m_xPosition, m_transform.position.y, position.z + zOffset);
}
