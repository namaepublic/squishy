﻿using MightyAttributes;
using UnityEngine;
using DG.Tweening;

public class BallBehaviour : MonoBehaviour
{
    private enum BallState
    {
        Idle,
        Bouncing,
        Falling
    }

    [Title("Movement")] public Vector3 startPosition;
    public float moveSpeed;
    [SerializeField] private float _dragSpeed;

    [Title("Bounce")] public float bounceDistance;
    public float bounceHeight;
    [SerializeField] private GameObject _sparklesParticle;

    [Title("Fall")] public float fallSpeed;
    public float fallDistance;
    public float fallHeight;

    [Title("Raycast")] [SerializeField] private Vector3 _rayOffset;
    [SerializeField] private float _rayLength;
    [SerializeField] private LayerMask _platformMask;
    [SerializeField, LayerField] private int _winningPlatformLayer;

    private int m_bounceCount;

    private float m_bounceDuration;
    private float m_fallDuration;

    private bool m_dragging;
    private float m_previousMouseX;
    private float m_dragDirection;

    private BallState m_state;

    private Tweener m_bounceTweener;
    private Tweener m_fallTweener;

    private Transform m_transform;
    
    public Vector3 Init()
    {
        m_transform = transform;
        
        m_state = BallState.Idle;

        m_previousMouseX = 0;
        m_bounceCount = 0;

        m_bounceDuration = bounceDistance / moveSpeed;
        m_fallDuration = fallDistance / fallSpeed;


        m_transform.DOComplete();
        return m_transform.position = startPosition;
    }

    public Vector3 UpdateBall(float deltaTime)
    {
        switch (m_state)
        {
            case BallState.Falling:
                if (!m_fallTweener.active)
                {
                    m_transform.DOComplete();
                    GameManager.Instance.Pause();
                    GameManager.Instance.ReloadLevel();
                }

                return m_transform.position;
            case BallState.Idle:
                Bounce();
                break;

            case BallState.Bouncing:
                if (!m_bounceTweener.active) TryBounce(m_transform.position);
                break;
        }

        if (ManageInput())
            return m_transform.position += Vector3.right * (m_dragDirection * _dragSpeed * deltaTime);

        return m_transform.position;
    }

    private bool ManageInput()
    {
        if (Input.GetMouseButtonDown(0))
            m_previousMouseX = Input.mousePosition.x;
        else if (Input.GetMouseButtonUp(0))
            m_dragDirection = 0;

        if (!Input.GetMouseButton(0)) return false;

        var currentMouseX = Input.mousePosition.x;
        m_dragDirection = currentMouseX - m_previousMouseX;
        m_previousMouseX = currentMouseX;

        return true;
    }

    private void Bounce()
    {
        m_state = BallState.Bouncing;

        m_bounceCount++;
        m_transform.DOMoveY(bounceHeight, m_bounceDuration / 2).SetLoops(2, LoopType.Yoyo);
        m_bounceTweener = m_transform.DOMoveZ(bounceDistance * m_bounceCount, m_bounceDuration).SetEase(Ease.Linear);
    }

    private void TryBounce(Vector3 position)
    {
        position = m_transform.position = new Vector3(position.x, 0, position.z);

        if (Physics.Raycast(GetRay(position), out var hit, _rayLength, _platformMask))
        {
            var hitTransform = hit.transform;

            if (hitTransform.gameObject.layer == _winningPlatformLayer)
            {
                m_transform.DOComplete();
                GameManager.Instance.Pause();
                GameManager.Instance.LoadNextLevel();
            }

            hitTransform.GetComponent<PlatformBehaviour>().Land();
            _sparklesParticle.SetActive(true);

            Bounce();
        }
        else
            Fall();
    }

    private void Fall()
    {
        m_state = BallState.Falling;

        m_fallTweener = m_transform.DOMoveY(fallHeight, m_fallDuration);
        m_transform.DOMoveZ(bounceDistance * m_bounceCount + fallDistance, m_fallDuration).SetEase(Ease.Linear);
    }

    private Ray GetRay(Vector3 position) => new Ray(position + _rayOffset, Vector3.down);

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        var ray = GetRay(transform.position);
        Debug.DrawRay(ray.origin, ray.direction * _rayLength, Color.red);
    }
#endif
}