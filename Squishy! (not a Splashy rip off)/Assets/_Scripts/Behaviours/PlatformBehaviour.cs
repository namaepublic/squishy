﻿using UnityEngine;

public class PlatformBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject _landParticle;
    
    public void Land() => _landParticle.SetActive(true);
}
