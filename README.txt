--------------------------------
--------------------------------
Squishy! (not a Splashy rip off)
--------------------------------
--------------------------------



-------------------------------
V1
--------------------------------
J'ai effectué ce test en 1h (+/- 5min), c'est pourquoi je me suis permis de prendre le temps d'appliquer quelques materials et de fine-tune les paramètres pour que le feeling ne soit pas mauvais. Il y a évidemment des choses à améliorer, mais pour un petit prototype je pense que c'est correct.

J'ai décidé de ne pas intégrer Dotween dans ce test, car je n'ai jamais utilisé ce plugin dans mes projets.
Bien qu'il a l'air très puissant et très pratique, j'ai préféré ne pas prendre le risque de faire des choses avec lesquelles j'aurais pu avoir de problèmes que je n'aurais probablement pas su gérer dans le temps imparti.
J'ai donc choisi d'effectuer le test en utilisant des choses que je maitrise déjà, pour éviter tout imprévu et rester le plus productif possible.

J'ignore si vous souhaitez plus d'information sur la réalisation de ce test, je suppose qu'on en discutera plus tard si mon prototype vous a convaincu, d'ici là n'hésitez pas à me contacter par email pour plus de détails.
--------------------------------



--------------------------------
V2
--------------------------------
Pour cette V2 j'ai donc intégré DOTween, j'aurais sûrement pu faire plus avec, mais vu que c'est ma première utilisation de ce plugin je m'en suis servis exclusivement pour les mouvements de la balle.
J'en ai profité pour améliorer plusieurs choses :
- C'est visuellement plus propre, j'ai utilisé une skybox et un shader d'eau trouvés sur l'asset store et j'ai ajouté des particles.
- J'ai ajouté 2 nouveaux levels, et j'en ai donc profité pour faire un système de chargement de level correct (et modèle que j'ai l'habitude d'utilisé, avec de l'additive scene loading).
- Un tout petit peu d'UX, avec un écran de pause entre chaques levels.
- Une sauvegarde de la progression, ici aussi avec un script que j'ai écris dans le passé et que j'ai l'habitude d'utiliser régulièrement.

Et j'ai également fait 2 builds ; 1 pour Windows et 1 pour WebGL.

J'ai passé environ 4h sur cette version, en incluant le temps à me documenter sur DOTween.

J'ai oublié de le mentionner pour la V1 (et durant l'entretien), mais dans ces deux versions j'ai également intégré [Mighty]Attributes, un plugin d'attributs C# sur lequel je travaille depuis assez longtemps, dont le but est de rendre l'Editor Scripting plus simple à l'aide d'une grande quantité d'attributs divers, c'est un peu Naughty Attributes en bien plus abouti, ou encore Odin Inspector mais cette fois-ci est moins avancé.
--------------------------------